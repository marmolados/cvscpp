/// C++ power test file

#include "cpp_power.hpp"

#pragma optimize( "", off )

void cpp_TestOne(int count) {
	char strBuff[1024];
	printString(CPP_POWER_Message);
	ticks start, stop;
	stopwatch((bigint*)&start);
	testOne(count);
	stopwatch((bigint*)&stop);
	stringSprint(strBuff, sizeof(strBuff), C_VS_CPP_ElapsedFormat, (stop - start));
	printString(strBuff);
}

#pragma optimize( "", on )