/// Main CvsCPP program file

#include "c_main.h"

initOS

#pragma optimize( "", off )

int main(int argCount, const char **argValues) {
	int testCount = C_VS_CPP_TestCount;
	printString(C_VS_CPP_StartMessage C_VS_CPP_InfoMessage);
	pressKey();
	printString(C_VS_CPP_GenericMessage C_VS_CPP_TestOneMessage(1, C_VS_CPP_TestCount));
	c_TestOne(testCount);
	cpp_TestOne(testCount);
	testCount *= C_VS_CPP_Multipler;
	printString(C_VS_CPP_TestOneMessage(2, (C_VS_CPP_TestCount * C_VS_CPP_Multipler)));
	c_TestOne(testCount);
	cpp_TestOne(testCount);
	printString(C_VS_CPP_EndTestMessage);
	return NoError;
}

#pragma optimize( "", on )