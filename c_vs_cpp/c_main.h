#ifndef __C_MAIN__
#define __C_MAIN__

/// includes
#include "c_os.h"
#include "c_msg.h"
#include "c_power.h"
#include "cpp_power.hpp"

// defines
#ifdef _DEBUG
#define C_VS_CPP_TestCount 2000
#define C_VS_CPP_Multipler 1.5
#else
#define C_VS_CPP_TestCount 3000
#define C_VS_CPP_Multipler 3
#endif

#endif /* __C_MAIN__ */