/// C power test file

#include "c_power.h"

initOS // should be for C init too

#pragma optimize( "", off )

void c_TestOne(int count) {
	char strBuff[1024];
	printString(C_POWER_Message);
	ticks start, stop;
	stopwatch(&start);
	testOne(count);
	stopwatch(&stop);
	stringSprint(strBuff, sizeof(strBuff), C_VS_CPP_ElapsedFormat, (stop - start));
	printString(strBuff);
}

#pragma optimize( "", on )