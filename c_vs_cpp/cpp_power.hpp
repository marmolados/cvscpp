#ifndef __CPP_POWER__
#define __CPP_POWER__

#include "c_os.h"
#include "c_msg.h"

#pragma optimize( "", off )

template <typename T>
inline T const& testOne(T const& n) {
	return ((n > 0) ?
#ifdef _DEBUG
		((n * testOne<T>(n - 1)) - n)
#else
		((n * testOne<T>(n - 1) /*+ testOne(n - 1)*/) - n)
#endif
		: 0);
}

#pragma optimize( "", on )

extern void cpp_TestOne(int count);

#endif /* __CPP_POWER__ */