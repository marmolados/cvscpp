/// Main messages definition

#ifndef __C_MSG__
#define __C_MSG__

/// CvsCPP messages
#define C_VS_CPP_VersionMessage "0.9.0.1"
#define C_VS_CPP_StartMessage "C vs. C++ ver. " C_VS_CPP_VersionMessage " performance counter by Marmos.\n" \
	"Copyright (c) " __DATE__ " NoN-Profit Yet Software, Inc. All rights reserved.\n\n"
#define C_VS_CPP_InfoMessage "This facility is dedicated mainly for performance neerds.\n" \
	"If you want to see what's doing press enter to continue... (Ctrl+C to break)\n"
#define C_VS_CPP_STR_(x) #x
#define C_VS_CPP_STR(x) C_VS_CPP_STR_(x)
#define C_VS_CPP_TestOneMessage(number, count) "Test number " C_VS_CPP_STR(number) " - (" C_VS_CPP_STR(count) " repeats).\n"
#define C_VS_CPP_GenericMessage "Recursive in C and Generic Programming C++.\n" \
	"Hold on a second...\n"
#define C_VS_CPP_TestTwoMessage "Recursive in C and Metaprogramming C++.\n" \
	"Hold on a second...\n"
#define C_VS_CPP_EndTestMessage "All tests completed!\n"

#define C_VS_CPP_ElapsedFormat "Elapsed ticks: %lu\n"

#define C_POWER_Message "Speed test for C...\n"
#define CPP_POWER_Message "Speed test for C++...\n"

#endif /* __C_MSG__ */