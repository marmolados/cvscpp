#ifndef __C_OS__
#define __C_OS__

#if defined WIN32 || defined _WIN64
/// Main OS includes
#include <Windows.h>
#include <Shlwapi.h>

/// Required libraries
#pragma comment (lib, "Shlwapi.lib")

/// Windows useful variables
extern HANDLE __hWinOutStdHandle__;
extern HANDLE __hWinInStdHandle__;
extern DWORD __dwTemporaryValue__;

/// Main OS defines
#define NoError NO_ERROR
#define printString(stringMessage) \
	if((__hWinOutStdHandle__ != INVALID_HANDLE_VALUE) || ((__hWinOutStdHandle__ = GetStdHandle(STD_OUTPUT_HANDLE)) != INVALID_HANDLE_VALUE)) \
		WriteConsoleA(__hWinOutStdHandle__, stringMessage, lstrlenA(stringMessage), &__dwTemporaryValue__, NULL)
#define pressKey() \
	if((__hWinInStdHandle__ != INVALID_HANDLE_VALUE) || ((__hWinInStdHandle__ = GetStdHandle(STD_INPUT_HANDLE)) != INVALID_HANDLE_VALUE)) \
		ReadConsoleA(__hWinInStdHandle__, NULL, 0, &__dwTemporaryValue__, NULL)
#define stopwatch QueryPerformanceCounter
#define stringSprint wnsprintfA

#define initOS \
HANDLE __hWinOutStdHandle__ = INVALID_HANDLE_VALUE; \
HANDLE __hWinInStdHandle__ = INVALID_HANDLE_VALUE; \
DWORD __dwTemporaryValue__ = 0;

#define ticks DWORDLONG
#define bigint LARGE_INTEGER

#ifndef inline
#if (_MSC_FULL_VER /*> 1800*/)  // It's bugged when a new version of VS using old toolset
// #define inline inline
#define inline __inline /* this should work for all VS/VC! */
#else 
#define inline inline
#endif
#endif /* inline */
	
#else
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define NoError 0L
#define printString(stringMessage) printf("%s", stringMessage)
#define pressKey() getchar()
#ifndef EXTERN_C
#ifdef __cplusplus
#define EXTERN_C    extern "C"
#else
#define EXTERN_C    extern
#endif
#endif /* EXTERN_C */

#define initOS

#define ticks unsigned long long
#define bigint ticks

#ifndef nsec_per_sec
#define nsec_per_sec 1000000000
#endif /* nsec_per_sec */

inline void stopwatch(ticks *count) {
	ticks nsec_count, nsec_per_tick;
	struct timespec ts1, ts2;
	
	clock_gettime(CLOCK_MONOTONIC, &ts1);
	nsec_count = ts1.tv_nsec + ts1.tv_sec * nsec_per_sec;
	clock_getres(CLOCK_MONOTONIC, &ts2);
	nsec_per_tick = ts2.tv_nsec + ts2.tv_sec * nsec_per_sec;
	
	*count = (nsec_count / nsec_per_tick);
}

#define stringSprint snprintf

#endif /* WIN32 */

#endif /* __C_OS__ */
