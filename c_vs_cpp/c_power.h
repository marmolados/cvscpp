#ifndef __C_POWER__
#define __C_POWER__

#include "c_os.h"
#include "c_msg.h"

#pragma optimize( "", off )

inline const int testOne(const int n) {
	return ((n > 0) ? 
#ifdef _DEBUG
		((n * testOne(n - 1)) - n)
#else
		((n * testOne(n - 1) /*+ testOne(n - 1)*/) - n)
#endif
		: 0);
}

#pragma optimize( "", on )

EXTERN_C void c_TestOne(int count);

#endif /* __C_POWER__ */
